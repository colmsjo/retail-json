//
// Retail-json.js
//
// Small, concice, modular and extensible JSON definitions for Retail 
// messaging. It is not a full specification, it is more of a template to
// be used when defining new interfaces. These messages can be used in any 
// language even though this is written in JavaScript. It is easlily converted
// to XML, see example at the end.
//
// Available messages:
// * Item
// * Party
//
// See code example at the end.
//
// TODO:
//  * More messages (Order, Despatch advice, Invoice etc.)
//  * Extend item with more information - vendor, manufacturer, first/last
//    order/selling date, min/max order quantities etc.
//  * A function that validates that the messages adhere to the templates might
//    be handy.
//
// LICENSE
//-------------------------------------------------------------------------
// Copyright (c) 2013 Gizur AB
//
// Permission is hereby granted, free of charge, to any person obtaining a 
// copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the 
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included 
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
// DEALINGS IN THE SOFTWARE.
//

// Common
//-----------------------------------------------------------------------------

// type can be GTIN, GLN  etc.
var identifier = {
	type : '',
	value : ''
};

// Can be used for any type of attributes, colors, sizes, etc.
//  type = 'color', value : 'red' 
var attribute = {
	type : '',
	value : ''
};


// Item
//-----------------------------------------------------------------------------

// uom = unit of measure (piece, pack, pallet etc.)
// identifier can be gtin etc.
var item = {
	item : {
		identifier : [],
		description : [],
		uom : '',
		barcode : [],
		unitType : [],
		dimension : [],
		attribute : [],
		itemsQty : []
	}
};

// item should be a item object and qty a interger
var itemQty = {
	item : null,
	qty : 0
};

// language should be the two letter ISO code
// Example {type : 'short description', value : 'Our diapers 20 pcs.', language : 'EN' }
var description = {
	type : '',
	value : '',
	language : ''	
};

// For instance { name : 'height', value : 10.0 }
var dimension = {
	name : '',
	value : 0.0
};

// for instance { type : 'EAN13', value : '0075678164125' }
var barcode = {
	type : '',
	value : ''
};

// Example of types: base, consumer, depatch, invoice, orderable etc.
// value: true and false
var unitType = {
	type : '',
	value : ''
};


// Party
//-----------------------------------------------------------------------------

// Used for all types of parties, i.e. customers, vendors etc.
// There can be many levels, exmaple HQ->[factory1, facotyr2, warehouse]
var party = {
	party : {
		active : true,
		identifier : [],
		adress : [],
		contact: [],
		attribute : [],
		party : []
	}
};

// Coutry ISO code is suggested for country
var adress = {
	name : '',
	street1 : '',
	street2 : '',
	poBox : '',
	postalCode : '',
	city : '',
	area : '',
	country : ''
};

// type can for instance be EMAIL, PHONE, PAGER ETC.
// Coutry ISO code is suggested for country
var contact = {
	type : '',
	value : '',
	name : '',
	country : ''
};


// Purchase prices
//-----------------------------------------------------------------------------

// currency should be currency ISO code
// market should be country ISO code
var purchaseCondition = {
    purchaseCondition : {

        relationship : {
            buyer : {
                identifier : []
            }, 
            seller : {
                identifier : []
            },
            effectiveFrom : '',
            effectiveTo : '',
            currency : '',
            market : '',
            incoTerms : []
        },

        itemCondition : [],

        orderCondition : [],

        freightCondition : []
    }
};


// Condition identifier is a item id, for instance GTIN
// Example condition distribution is DC
// Either price or allowance should be used
var itemCondition = {
    valueBasis : [],
    qualifier : [],
    identifier: [],
    price : [],
    allowance : [],
    distribution : ''
};

var orderCondition = {
    valueBasis : [],
    qualifier : [],
    allowance : []
};

var freightCondition = {
    valueBasis : [],
    qualifier : [],
    price : [],
    allowance : [],
    distribution : ''
};

// See ICC INCOTERMS 2010
var incoTerms = {
    code: '',
    codeLocation : '',
};


// Examples of unitOfMeasure: WEEK, MONTH, YEAR etc.
var valueBasis = {
    unitOfMeasure : '',
    value: 0
};


// operators: AND and OR
var qualifier = {
    operator : '',
    UOM : '',
    min : 0,
    max : 0
};


// currency should be a currency ISO code
var price = {
    value : 0.0,
    currency : ''
};


// Examples of types: PERCENTAGE, AMOUNT
var allowance = {
    type : '',
    value : 0.0,
    currency : ''
};



// Examples
//-----------------------------------------------------------------------------
//
// Run this example like this (nodejs needs to be installed, http://nodejs.org/):
//
// `npm install`
// `node retail-json.js`
//

// Example of usage
var exampleOfUsage = function() {

	var trousers = {
		item : {
			identifier : [{
				type : 'GTIN',
				value : '0123456789012'
			}],
			description : [
			    { type : 'short description',
				  value : 'Blue Jeans size 32/34',
				  language : 'EN'
				}],
			uom : 'PIECE',
			barcode :[{
				type: 'EAN13', 
				value : '0123456789012'
			}],
			unitType :[{
				type : 'SELLABLE',
				value : true
			}]
		}
	};

	var trouserPack = {
		item : {
			identifier : [{
				type : 'GTIN',
				value : '0123456789012'
			}],
			uom : 'PACK',
			barcode :[{
				type: 'GS1-128', 
				value : '(00)00012345678905'
			}],
			unitType :[{
				type : 'ORDERABLE',
				value : true
			}],
			itemsQty : [{
				item : trousers,
				qty : 4
			}],
			dimension : [
			     { name : 'height', value: 500 },
			     { name : 'width', value: 500 },
			     { name : 'depth', value: 500 },
			     { name : 'weight', value: 10 }
			]
		}
	};
	
	// just make sure it is serializable (probably not necessary)
	JSON.parse(JSON.stringify(trouserPack));

	// print to console
	console.log(JSON.stringify(trouserPack));

	// If you prefer XML
	var jsonxml = require('jsontoxml');

	var xml = jsonxml(trouserPack);
	console.log(xml);

}

exampleOfUsage();

